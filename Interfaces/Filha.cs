﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Filha : PaiInterface
    {
        public void gravar(string campo1, string campo2)
        {
            Console.WriteLine("Campo 1 = " + campo1);
            Console.WriteLine("Campo 2 = " + campo2);
        }
    }
}
